var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0],
  medium: [128, 128, 128],
  main: [
    [255, 255, 255],
    [128, 128, 128],
    [64, 64, 64]
  ]
}

var arrays = [create2DArray(16, 16, 0, true), create2DArray(16, 16, 0, true), create2DArray(16, 16, 0, true)]
var spreaders = [{
    x: 7,
    y: 7
  },
  {
    x: 8,
    y: 7
  },
  {
    x: 7,
    y: 8
  }
]
var neighbours = [
  [],
  [],
  []
]
var ratio = [0.85, 0.95, 0.90]

var frames = 0
var boardSize

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  colorMode(RGB, 255, 255, 255, 1)
  rectMode(CENTER)
  // background rectangle
  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)
  // grid points
  for (var i = 0; i < 17; i++) {
    for (var j = 0; j < 17; j++) {
      fill(colors.medium)
      noStroke()
      ellipse(windowWidth * 0.5 + (i - 8) * (42 / 768) * boardSize, windowHeight * 0.5 + (j - 8) * (42 / 768) * boardSize, 3, 3)
    }
  }
  // drawing spreaders
  for (var i = 0; i < arrays.length; i++) {
    for (var j = 0; j < arrays[i].length; j++) {
      for (var k = 0; k < arrays[i][j].length; k++) {
        if (arrays[i][j][k] > 0) {
          fill(colors.main[i][0], colors.main[i][1], colors.main[i][2], 0.75)
          noStroke()
          rect(windowWidth * 0.5 + (j - 7.5) * (42 / 768) * boardSize, windowHeight * 0.5 + (k - 7.5) * (42 / 768) * boardSize, Math.floor((arrays[i][j][k] / 1.25) * 3 + 1) * (32 / 768) * boardSize / 3, Math.floor((arrays[i][j][k] / 1.25) * 3 + 1) * (32 / 768) * boardSize / 3)
        }
      }
    }
  }
  // updating spreaders
  frames += deltaTime * 0.025
  if (frames > 1) {
    frames = 0

    for (var i = 0; i < arrays.length; i++) {
      arrays[i][spreaders[i].x][spreaders[i].y] = 1
      neighbours[i] = getNeighbours(arrays[i], ratio[i])
      for (var j = 0; j < neighbours[i].length * 0.5; j++) {
        var rand = Math.floor(Math.random() * neighbours[i].length)
        if (neighbours[i][rand] !== undefined) {
          spreaders[i].x = neighbours[i][rand][0]
          spreaders[i].y = neighbours[i][rand][1]
          arrays[i][spreaders[i].x][spreaders[i].y] = 1
        }
      }
      for (var j = 0; j < arrays[i].length; j++) {
        for (var k = 0; k < arrays[i][j].length; k++) {
          if (arrays[i][j][k] > 0) {
            arrays[i][j][k] -= 0.019 + (mouseX * 0.1) / windowWidth
          } else {
            arrays[i][j][k] = 0
          }
        }
      }
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}

function getNeighbours(array, p) {
  var neighbour = []
  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      if (array[i][j] > p) {
        if (array[(i + 1) % array.length][j] === 0) {
          neighbour.push([(i + 1) % array.length, j])
        }
        if (array[(array.length + (i - 1)) % array.length][j] === 0) {
          neighbour.push([(array.length + (i - 1)) % array.length, j])
        }
        if (array[i][(j + 1) % array.length] === 0) {
          neighbour.push([i, (j + 1) % array.length])
        }
        if (array[i][(array.length + (j - 1)) % array.length] === 0) {
          neighbour.push([i, (array.length + (j - 1)) % array.length])
        }
      }
    }
  }
  return neighbour
}
